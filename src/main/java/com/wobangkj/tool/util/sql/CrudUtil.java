package com.wobangkj.tool.util.sql;

import com.wobangkj.tool.api.result.*;
import com.wobangkj.tool.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * @author dreamlu
 * @des 通用增删改查
 */
@Slf4j
public class CrudUtil {

    /**
     * des: JpaRepository repository 属性不抽取, 防止并发引用, 全局问题
     */
    // search
    @SuppressWarnings("Duplicates")
    public static Object search(Map<String, Object> params, Object data, JpaRepository repository) {

        try {
            // 创建 ExampleMatcher
            // 过滤查询条件, 但不会拦截返回结果
            ExampleMatcher exampleMatcher = ExampleMatcher.matching()
                    // 忽略 id 和 createTime 字段。
                    //.withIgnorePaths("id")
                    // 忽略大小写。
                    .withIgnoreCase()
                    // 忽略为空字段。
                    .withIgnoreNullValues();

            // 携带 ExampleMatcher。
            Example<Object> example = Example.of(data, exampleMatcher);

            // 分页查询，从 0 页开始查询 everyPage 个。
            Integer clientPage;
            Integer everyPage;
            Page page;

            // every参数判断
            // 返回所有, 不分页
            String every = (String) params.get("every");
            // 查询所有数据
            if ("all".equals(every)) {
                List content = repository.findAll(example, Sort.by(Sort.Direction.DESC, "id"));
                if (content.size() == 0) {
                    return Result.MapNoResult;
                }
                return Result.GetMapData(Result.CodeSuccess, Result.MsgSuccess, content);
            } else {
                // 分页查询，从 0 页开始查询 everyPage 个。
                clientPage = Integer.parseInt((String) params.get("clientPage"));
                everyPage = Integer.parseInt((String) params.get("everyPage"));
                page = repository.findAll(example, PageRequest.of(clientPage - 1, everyPage, Sort.by(Sort.Direction.DESC, "id")));
            }

            // 分页表
            List content = page.getContent();
            if (content.size() == 0) {
                return Result.GetMapData(Result.CodeNoResult, Result.MsgNoResult);
            }

            // ignore参数判断v1
            // 反射原理
            // 返回数据拦截处理(将值赋值为null)
			/*String ignore = (String) params.get("ignore");
			if (ignore != null) {
				String[] ignores = ignore.split(",");
				for (Object data : content) {
					for (String field : ignores) {
						Util.setFieldValue(field, null, data);
					}
				}
			}*/

            // ignore参数判断v2
            // 第三方json包
            // 返回数据拦截处理(将字段直接删除)
            // 小bug, 将json中null字段同时删除
            // JSONObject jsonObject=new JSONObject(content);
            // JSONArray jsonArray= jsonObject.getJSONArray(null);
            String ignore = (String) params.get("ignore");
            if (ignore != null && ignore.length() != 0 && content.size() != 0) {
                String[] ignores = ignore.split(",");
                JSONArray jsonArray = new JSONArray(content);
                // Iterator<String> iterator = ignores
                for (String field : ignores) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonData = jsonArray.getJSONObject(i);//得到对象中的第i条记录
                        jsonData.remove(field);
                    }
                }
                content = jsonArray.toList();
            }

            // 总数量 替换 总页数
            // 针对一些接口需要统计总数量问题, 不必重写接口
            long sumPage = page.getTotalElements();//.getTotalPages();

            return new GetInfoPager<Object>(Result.CodeSuccess, Result.MsgSuccess, content, new Pager(clientPage, (int) sumPage, everyPage));
        } catch (IllegalArgumentException ex) {
            return Result.GetMapData(Result.CodeText, Result.MsgArgsErr);
        } catch (EmptyResultDataAccessException ex) {
            return Result.GetMapData(Result.CodeText, Result.MsgNoData);
        } catch (Exception e) {
            // 暂无数据
            if (e.getCause().getClass() == EntityNotFoundException.class) {
                return Result.MapNoResult;
            }

            //throw e;
            return Result.GetMapData(Result.CodeSql, e.getMessage());
        }
    }

    // get by id
    // create
    public static Object getById(Object id, JpaRepository repository) {

        try {
            Object data = repository.findById(id).get();
            return Result.GetMapData(Result.CodeSuccess, Result.MsgSuccess, data);
        } catch (NoSuchElementException ex) {
            return Result.GetMapData(Result.CodeText, Result.MsgNoData);
        } catch (Exception e) {
            return Result.GetMapData(Result.CodeSql, e.getCause().getCause().getMessage());
        }
    }

    // delete
    public static MapData delete(Object id, JpaRepository repository) {

        try {
            repository.deleteById(id);
            return Result.MapDelete;
        } catch (EmptyResultDataAccessException ex) {
            return Result.GetMapData(Result.CodeText, Result.MsgNoData);
        } catch (Exception e) {
            return Result.GetMapData(Result.CodeSql, e.getCause().getCause().getMessage());
        }
    }

    // update
    public static MapData update(Object data, JpaRepository repository) {
        try {
            Object udateData = repository.save(data);
            Object id = Util.getFieldValue("id", udateData);
            if (id == null) {
                return Result.GetMapData(Result.CodeText, "id不能为空");
            }
            return Result.MapUpdate;
        } catch (NoSuchElementException e) {
            return Result.GetMapData(Result.CodeSql, "条件值不存在");
        } catch (Exception e) {
            String msg = e.getCause().getCause().getMessage();
            if (msg.contains("Duplicate entry")) {
                String key = msg.substring(msg.indexOf("key '") + 5, msg.length() - 1);
                int code = isChinese(key) ? Result.CodeText : Result.CodeSql;
                return Result.GetMapData(code, key);
            }
            return Result.GetMapData(Result.CodeSql, msg);
        }
    }

    // create
    // return id
    public static MapData create(Object data, JpaRepository repository) {

        try {
            Object createData = repository.save(data);
            return Result.GetMapData(Result.CodeCreate, Result.MsgCreate, new HashMap<String, Object>() {
                {
                    put("id", Util.getFieldValue("id", createData));
                }
            });
        } catch (Exception e) {
            String msg = e.getCause().getCause().getMessage();
            if (msg.contains("Duplicate entry")) {
                String key = msg.substring(msg.indexOf("key '") + 5, msg.length() - 1);
                int code = isChinese(key) ? Result.CodeText : Result.CodeSql;
                return Result.GetMapData(code, key);
            }
            return Result.GetMapData(Result.CodeSql, e.getCause().getCause().getMessage());
        }
    }

    // 完整的判断中文汉字和符号
    public static boolean isChinese(String strName) {
        char[] ch = strName.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            char c = ch[i];
            if (isChinese(c)) {
                return true;
            }
        }
        return false;
    }

    // 根据Unicode编码完美的判断中文汉字和符号
    private static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {
            return true;
        }
        return false;
    }
}
