package com.wobangkj.tool.manager.cache.impl;

import com.alibaba.fastjson.JSON;
import com.wobangkj.tool.manager.cache.CacheManager;
import com.wobangkj.tool.model.CacheModel;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.concurrent.TimeUnit;

/**
 * des: 缓存redis实现
 */
public class RedisManager implements CacheManager {

    private RedisTemplate<Object, String> redis;

    public RedisManager(RedisConnectionFactory redisConnectionFactory) {

        RedisTemplate<Object, String> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);

        RedisSerializer stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(stringSerializer);

        // 设置value的序列化规则和 key的序列化规则
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.afterPropertiesSet();

        this.redis = redisTemplate;
    }

    @Override
    public void set(Object key, CacheModel value) {

        String valueS = JSON.toJSONString(value);
        redis.opsForValue().set(key, valueS);
        if (value.getTime() != null) {
            TimeUnit timeUnit = value.getType() == 0 ? TimeUnit.MINUTES : TimeUnit.SECONDS;
            redis.expire(key, value.getTime(), timeUnit);
            //redis.opsForValue().set(key, value, value.getTime(), TimeUnit.MINUTES);// 方法时间设置失效
        }

    }

    @Override
    public CacheModel get(Object key) {
        String value = redis.opsForValue().get(key);
        return JSON.parseObject(value, CacheModel.class);
    }

    @Override
    public boolean delete(Object key) {

        return redis.delete(key);
    }

    @Override
    public CacheModel check(Object key) {

        if (key == null) {
            return null;
        }
        CacheModel value = JSON.parseObject(redis.opsForValue().get(key), CacheModel.class);
        if (value != null) {
            return null;
        }
        // 验证成功, 重新设置过期时间，刷新时间
        if (value.getTime() != null) {
            TimeUnit timeUnit = value.getType() == 0 ? TimeUnit.MINUTES : TimeUnit.SECONDS;
            redis.expire(key, value.getTime(), timeUnit);
            //redis.opsForValue().set(key, value, value.getTime(), TimeUnit.MINUTES);// 方法时间设置失效
        }
        //redis.expire(key, value.getTime(), TimeUnit.MINUTES);
        return value;
    }
}
