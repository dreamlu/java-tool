package com.wobangkj.tool.api.result;

/**
 * 状态码以及常用操作
 */

//final修饰符
public final class Result {

    // 约定状态码
    public static final int
            CodeSuccess = 200, // 请求成功
            CodeCreate = 201, // 创建成功
            CodeNoAuth = 203, // 请求非法
            CodeNoResult = 204, // 暂无数据
            CodeUpdate = 206, // 修改成功
            CodeDelete = 209, // 删除成功
            CodeValidate = 217, // 验证成功
            CodeValidateErr = 218, // 验证失败
            CodeSql = 222, // 数据库错误统一状态
            CodeLackArgs = 223, // 缺少参数
            CodeFile = 224, // 文件上传相关
            CodeWx = 240, // 微信小程序相关
            CodeText = 271, // 全局文字提示
            CodeNoRoute = 404, // 路由不存在
            CodeError = 500; // 系统繁忙

    // 约定提示信息
    public static final String
            MsgSuccess = "请求成功",
            MsgCreate = "创建成功",
            MsgNoAuth = "请求非法",
            MsgNoResult = "暂无数据",
            MsgDelete = "删除成功",
            MsgUpdate = "修改成功",
            MsgError = "未知错误",
            MsgLackArgs = "缺少参数",
            MsgValidate = "验证成功",
            MsgValidateErr = "验证码错误",
            MsgFile = "上传成功",
            MsgNoData = "数据不存在",
            MsgArgsErr = "参数错误";

    // 约定提示信息
    public static final MapData
            MapSuccess = new MapData(CodeSuccess, MsgSuccess),          // 统一成功
            MapError = new MapData(CodeError, MsgError),              // 统一错误
            MapUpdate = new MapData(CodeUpdate, MsgUpdate),            // 修改成功
            MapDelete = new MapData(CodeDelete, MsgDelete),            // 删除成功
            MapCreate = new MapData(CodeCreate, MsgCreate),            // 创建成功
            MapNoResult = new MapData(CodeNoResult, MsgNoResult),        // 暂无数据
            MapNoAuth = new MapData(CodeNoAuth, MsgNoAuth),            // 请求非法
            MapLackArgs = new MapData(CodeLackArgs, MsgLackArgs),        // 缺少参数
            MapValidate = new MapData(CodeValidate, MsgValidate),        // 验证成功
            MapValidateErr = new MapData(CodeValidateErr, MsgValidateErr);  // 验证成功

    // 信息通用,状态码及信息提示
    public static MapData GetMapData(Integer status, String msg) {
        return new MapData(status, msg);
    }

    // 信息通用,状态码及信息提示
    public static GetInfo GetMapData(Integer status, String msg, Object data) {
        return new GetInfo<>(status, msg, data);
    }

    // 信息通用,状态码及信息提示, 分页
    public static GetInfoPager GetMapDataPager(Object data, Integer clientPage, Integer sumPage, Integer everyPage) {
        return new GetInfoPager<>(Result.CodeSuccess, Result.MsgSuccess, data, new Pager(clientPage, sumPage, everyPage));
    }

    // 信息通用,状态码及信息提示, 分页
    @Deprecated
    public static GetInfoPager GetMapDataPager(Object data, Pager pager) {
        return new GetInfoPager<>(Result.CodeSuccess, Result.MsgSuccess, data, pager);
    }

    // 信息成功数据通用
    public static GetInfo GetMapDataSuccess(Object data) {
        return GetMapData(Result.CodeSuccess, Result.MsgSuccess, data);
    }

    // 信息失败数据通用
    public static GetInfo GetMapDataError(Object data) {
        return GetMapData(Result.CodeError, Result.MsgError, data);
    }

    // 信息成功数据通用
    public static MapData GetText(String data) {
        return GetMapData(Result.CodeText, data);
    }

}