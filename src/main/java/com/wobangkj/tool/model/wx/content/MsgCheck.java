package com.wobangkj.tool.model.wx.content;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dreamlu
 * @date 2019/11/28 上午10:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MsgCheck {

    private String content;

   // private String access_token;
}
